import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.stream.Collectors;

import interfaces.hpo.IndexHPO;
import interfaces.hpo.SearchHPO;
import interfaces.hpo.SearchHPOAnno;
import interfaces.omim.OmimTxtIndex;
import interfaces.omim.SearchOmimTxt;
import interfaces.orphadata.SearchOrphadata;
import interfaces.sider.SiderController;
import interfaces.stitch.IndexAtc;
import interfaces.stitch.IndexStitch;
import interfaces.stitch.SearchAtc;
import interfaces.stitch.SearchStitch;
import models.AtcEntry;
import models.AtcEntryList;
import models.Disease;
import models.DiseaseList;
import models.Drug;
import models.DrugList;
import models.HPOEntryList;
import models.HPOExternalIdList;
import models.OmimTxtEntry;
import models.OmimTxtEntryList;
import models.OrphaEntry;
import models.OrphaEntryList;
import models.SiderEntry;
import models.SiderEntryList;
import models.StitchEntry;
import models.StitchEntryList;



public class Program {
	
	
	public static DrugList siderToDrugs(SiderEntryList sel) {
		DrugList drugs = new DrugList(); 
		Drug d;
		
		for (SiderEntry se : sel) {
			
			try {
				int j = 0;
				StitchEntry  ste = SearchStitch.searchChemId(se.getStitchId());
				if (ste != null) {
					AtcEntry ae = SearchAtc.searchAtcId(ste.getAtcId());
					if (ae != null) {	
						if (drugs.getAllCuis().contains(se.getCui())) {
							for (int i = 0; i<drugs.size(); i++) {
								if (drugs.get(i).equals(se.getCui())) {
									j = i;
									break;
								}
							}
							
							drugs.get(j).addIndication(se.getIndication());
							drugs.get(j).addSideEffect(se.getSideEffect());
							drugs.get(j).setCui(se.getCui());
							
							drugs.get(j).addLabel(ae.getname());
							
						} else {
							d = new Drug();
							d.addIndication(se.getIndication());
							d.addSideEffect(se.getSideEffect());
							d.setCui(se.getCui());
							d.addLabel(ae.getname());						
							
							drugs.add(d);
						}
					}
				}			
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return drugs;		
	}
	
	public static DiseaseList mergeDiseases(OrphaEntryList oel, OmimTxtEntryList otel) {
		ArrayList<String> omimIds = otel.getAllIds();
		DiseaseList diseases = new DiseaseList();
		
		Disease d;
		OmimTxtEntry ote;
		for (OrphaEntry oe : oel) {
			d = new Disease();
			d.setOrphaId(oe.getOrphaNumber()+"");
			d.addSymptoms(oe.getClinicalSigns());
			d.addName(oe.getName());
			d.addDrugs(siderToDrugs(oe.getMeds()));
			
			for (int i = 0; i< otel.size(); i++) {
				ote = otel.get(i); 				
				if (oe.getOmimId() != null && oe.getOmimId().equals(ote.getId())) {
					d.setOmimId(ote.getId());
					omimIds.add(ote.getId());
					
					d.addNames(ote.getTitles());
					d.addSymptoms(ote.getClinicalSymptoms());
					d.addDrugs(siderToDrugs(ote.getMeds()));
					break;
				}				
			}
			
			diseases.add(d);
		}
		
		for (OmimTxtEntry ote2 : otel) {
			// si la maladie n'était pas en double dans omim et orpha
			if (!omimIds.contains(ote2.getId())) {
				d = new Disease();
				d.setOmimId(ote2.getId());
				d.addNames(ote2.getTitles());
				d.addSymptoms(ote2.getClinicalSymptoms());
				
				d.addDrugs(siderToDrugs(ote2.getMeds()));				
			}
		}
		
		return diseases;
	}
	
	
	
	public static void main(String[] args){
		
		//IndexHPO.index();
		//IndexStitch.index();
        //IndexAtc.index();
        //OmimTxtIndex.index();
		//OmimCsvIndex.index();
        
        HPOEntryList syns;
        
		try {
			SiderController.connect();
			
			String input = "";
			
			Scanner reader = new Scanner(System.in);
			System.out.println("Please enter your symptom");
			input = reader.nextLine();
			//input = "hypoglycemia";
			String entry = input; 
			
			System.out.println("Do you want to show:\nDiseases [1]\nMedication with side effects [2]");
			input = reader.nextLine();
			//input = "1";
			int option = Integer.parseInt(input);
			
			System.out.println("How many results do you want to display at maximum?");
			input = reader.nextLine();
			//input = "20";
			
			int N = Integer.parseInt(input);
			
			syns = SearchHPO.getNNames(entry,N);
			//System.out.println(syns);
			
			
			System.out.println();
			if (option == 1) {
				         	        
		        // initialise la connection à hpo_annotations.sqlite
		        SearchHPOAnno HPOanno = new SearchHPOAnno();
		        
		        // on cherche toute les correspondances à l'id HPO dans ORPHA et OMIM 
		        HPOExternalIdList extIds = HPOanno.getAllIds(syns.getAllIds());
		        HPOanno.close();		        
		        
	        	//on récupère les maladies dans ORPHA avec les ids récupéré dans HPO et leur nom
		        OrphaEntryList orphaEntries = SearchOrphadata.getDiseaseById(extIds.getOrphaIds().getAllIds());	        
		        orphaEntries.addAll(SearchOrphadata.getDiseaseByClinicalSign(syns.getAllNames()));
		        		        
		        orphaEntries = SearchOrphadata.removeDoubles(orphaEntries);		        		        		       
		        
		        
		        
		        for (int i = 0; i<orphaEntries.size() && i <N; i++) {
		        	if (!orphaEntries.get(i).getClinicalSigns().isEmpty()) {
		        		orphaEntries.get(i).addMeds(SiderController.getMedByIndications(orphaEntries.get(i).getClinicalSigns(), 5));
		        	}
		        }
		        
		        //System.out.println(orphaEntries);		        		       
		        
		        //on récupère les maladies dans OMIM avec les ids récupéré dans HPO et leur nom
		        OmimTxtEntryList otel = SearchOmimTxt.searchOmimId(extIds.getAllOmimIds().getAllIds());
		        otel.addAll(SearchOmimTxt.searchClinicalSymptom(syns.getAllNames()));
		        
		        otel = SearchOmimTxt.removeDoubles(otel);
		        
		        for (int i = 0; i<otel.size() && i <N; i++) {
		        	if (!otel.get(i).getClinicalSymptoms().isEmpty()) {
		        		otel.get(i).addMeds(SiderController.getMedByIndications(otel.get(i).getClinicalSymptoms(), 5));
		        		otel.get(i).addMeds(SiderController.getMedByIndications(otel.get(i).getTitles(), 5));
		        	}
		        }		        
		        //System.out.println(otel);
		        		        
		        DiseaseList dl = mergeDiseases(orphaEntries, otel);
		        
		        System.out.println("Displaying " + Math.min(N, dl.size()) + " results.");
		        System.out.println(dl.toString(N));
		        
			} else if (option == 2){
				
		        SiderEntryList sel = SiderController.getMedBySideEffects(syns.getAllNames(), N); 
		        		        
		        DrugList dl = siderToDrugs(sel);
		        
		        System.out.println("Displaying " + dl.size() + " results.");
		        
		        System.out.println(dl);
		        		        
			}
	        
	        reader.close();
	        SiderController.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
        

	}

}
