package models;

import java.util.ArrayList;

public class HPOEntry {

    private String id;
    private ArrayList<String> names;
    private ArrayList<String> altIds;
    
    public HPOEntry() {
        names = new ArrayList<String>();
        altIds = new ArrayList<String>();
    }
    
    public void addName(String name) {
        names.add(name);
    }
    
    public void addAltId(String altId) {
        altIds.add(altId);
    }
    
    public ArrayList<String> getNames() {
        return names;
    }
    public void setNames(ArrayList<String> names) {
        this.names = names;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public ArrayList<String> getAltIds() {
        return altIds;
    }
    public void setAlt_ids(ArrayList<String> alt_ids) {
        this.altIds = alt_ids;
    }
    
    @Override
    public String toString() {
        String string = "";
        
        string += "name : " + names.get(0) + "\n";
        string += "id : HP:" + id + "\n";
        
        for (int i = 1; i<names.size()-1; i++)
            string += "synonym : " + names.get(i) + "\n";
        
        for (int i = 0; i<altIds.size()-1; i++)
            string += "alt_id : " + names.get(i) + "\n";
        
        return string;
    }
    
}
