package models;

import java.util.ArrayList;

public class HPOEntryList extends ArrayList<HPOEntry>{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public ArrayList<String> getAllNames(){
        ArrayList<String> names = new ArrayList<String>();
        
        for (int i = 0; i<this.size()-1; i++)
            names.addAll(this.get(i).getNames());
        
        return names;
    }
    
    public ArrayList<String> getAllIds(){
        ArrayList<String> ids = new ArrayList<String>();
        
        for (int i = 0; i<this.size()-1; i++)
            ids.add(this.get(i).getId());
        
        return ids;
    }
    
    public ArrayList<String> getAllAltIds(){
        ArrayList<String> altIds = new ArrayList<String>();
        
        for (int i = 0; i<this.size()-1; i++)
            altIds.addAll(this.get(i).getAltIds());
        
        return altIds;
    }
    
    @Override
    public String toString() {
        String string = "";
        
        for (HPOEntry hpoe : this)
        	string += hpoe + "\n";
        
        return string;
    }

}
