package models;

public class OmimCsvEntry {

	String cui, omimId;

	public OmimCsvEntry(String cui, String omimId) {
		this.cui = cui;
		this.omimId = omimId;
	}

	public String getCui() {
		return cui;
	}

	public void setCui(String cui) {
		this.cui = cui;
	}

	public String getOmimId() {
		return omimId;
	}

	public void setOmimId(String omimId) {
		this.omimId = omimId;
	}
}
