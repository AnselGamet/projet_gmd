package models;

import utils.Util;

public class StitchEntry {
    String chemicalId, aliasId, atcId;

    public StitchEntry(String chemicalId, String aliasId, String atcId) {
        this.chemicalId = Util.capitalize(chemicalId, 0, 3);
        this.aliasId = Util.capitalize(aliasId, 0, 3);
        this.atcId = Util.capitalize(atcId);
    }

    public String getChemicalId() {
        return chemicalId;
    }

    public void setChemicalId(String chemicalId) {
        this.chemicalId = chemicalId;
    }

    public String getAliasId() {
        return aliasId;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }

    public String getAtcId() {
        return atcId;
    }

    public void setAtcId(String atcId) {
        this.atcId = atcId;
    }

    @Override
    public String toString() {
        return chemicalId + "\t" + aliasId + "\t" + atcId;
    }
    
    public boolean equals(StitchEntry se) {
    	return (this.chemicalId.equals(se.getChemicalId()) &&
    			this.aliasId.equals(se.getAliasId()) &&
    			this.atcId.equals(se.getAtcId()));
    }
}
