package models;

import java.util.ArrayList;

public class OmimCsvEntryList extends ArrayList<OmimCsvEntry>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArrayList<String> getAllOmimIds() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (OmimCsvEntry ote : this)
			results.add(ote.getOmimId());
		
		return results;
	}
	
	public ArrayList<String> getAllCuis() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (OmimCsvEntry ote : this)
			results.add(ote.getCui());
		
		return results;
	}
	
	@Override
	public String toString() {
		String string = "";
		for (OmimCsvEntry ote : this) {
			string += ote+"\n";
		}
		return string;
	}
	
}
