package models;

import java.util.ArrayList;

public class OrphaEntryList extends ArrayList<OrphaEntry>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArrayList<String> getAllOrphaNumbers() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (OrphaEntry oe : this)
			results.add(oe.getOrphaNumber() + "");
			
		return results;
	}
	
	public ArrayList<String> getAllCinicalSigns() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (OrphaEntry oe : this)
			results.add(oe.getClinicalSigns() + "");
			
		return results;
	}
	
	public ArrayList<String> getAllNames() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (OrphaEntry oe : this)
			results.add(oe.getName() + "");
			
		return results;
	}
	
	@Override
	public String toString() {
		String string = "";
		
		for (OrphaEntry oe : this)
			string += oe+"\n";
		
		return string;
	}
}
