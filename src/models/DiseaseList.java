package models;

import java.util.ArrayList;

public class DiseaseList extends ArrayList<Disease>{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		String string = "";
		
		for (Disease d : this)
			string += d + "\n";
		
		return string; 
	}
	
	public String toString(int N) {
		String string = "";
				
		for (int i = 0; i < this.size() && i<N; i++) {
			string += i+1+".\n";
			string += this.get(i) + "\n";
		}
		
		return string; 
	}
}
