package models;

import utils.Util;

public class AtcEntry {
    String name, atcId;

    public AtcEntry(String name,  String atcId) {
        this.name = name;
        this.atcId = Util.capitalize(atcId);
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getAtcId() {
        return atcId;
    }

    public void setAtcId(String atcId) {
        this.atcId = atcId;
    }

    @Override
    public String toString() {
        return atcId+ "\t" + name;
    }
    
}
