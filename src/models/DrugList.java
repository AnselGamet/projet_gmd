package models;

import java.util.ArrayList;

public class DrugList extends ArrayList<Drug> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArrayList<String> getAllCuis() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (Drug d : this)
			results.add(d.getCui());
		
		return results;
	}
	
	public ArrayList<String> getAllLabels() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (Drug d : this)
			results.addAll(d.getLabels());
		
		return results;
	}
	
	public ArrayList<String> getAllIndications() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (Drug d : this)
			results.addAll(d.getIndications());
		
		return results;
	}
	
	public ArrayList<String> getAllSideEffects() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (Drug d : this)
			results.addAll(d.getSideEffects());
		
		return results;
	}
	
	
	@Override
	public String toString() {
		String string = "";
		
		for (Drug d : this) 
			string += d + "\n";
		
		return string;
	}
}
