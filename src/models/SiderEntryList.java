package models;

import java.util.ArrayList;

public class SiderEntryList extends ArrayList<SiderEntry>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArrayList<String> getAllStitchId() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (SiderEntry se : this) {
			results.add(se.getStitchId());
		}
		
		return results;
	}
	
	public ArrayList<String> getAllCui() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (SiderEntry se : this) {
			results.add(se.getCui());
		}
		
		return results;
	}
	
	public ArrayList<String> getAllConceptNames() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (SiderEntry se : this) {
			results.add(se.getSideEffect());
		}
		
		return results;
	}
	
	
	@Override
	public String toString() {
		String string = "";
		for (SiderEntry se:this) {
			string += se + "\n";
		}
		return string;
	}
}
