package models;

import java.util.ArrayList;

public class AtcEntryList extends ArrayList<AtcEntry>{

	public ArrayList<String> getAllAtcId() {
		ArrayList<String> results = new ArrayList<String>();
		for (AtcEntry ae : this) {
			results.add(ae.getAtcId());
		}
		
		return results;
	}
	
	public ArrayList<String> getAllName() {
		ArrayList<String> results = new ArrayList<String>();
		for (AtcEntry ae : this) {
			results.add(ae.getname());
		}
		
		return results;
	}
	
	@Override
	public String toString() {
		String string = "";
		for (AtcEntry ae : this) {
			string += ae + "\n";
		}
		
		return string;
	}
	
}
