package models;

import java.util.ArrayList;

public class OrphaEntry {

    String name, omimId;
    ArrayList<String> clinicalSigns;
    SiderEntryList meds;
    int orphaNumber;    
    
    public OrphaEntry(String name, int orphaNumber) {
        this.name = name;
        this.orphaNumber = orphaNumber;
        clinicalSigns = new ArrayList<String>();
        meds = new SiderEntryList();
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public ArrayList<String> getClinicalSigns() {
        return clinicalSigns;
    }
    public void setClinicalSigns(ArrayList<String> clinicalSign) {
        this.clinicalSigns = clinicalSign;
    }
    public void addClinicalSigns(ArrayList<String> clinicalSign) {
        this.clinicalSigns.addAll(clinicalSign);
    }
    public void addClinicalSigns(String clinicalSign) {
        this.clinicalSigns.add(clinicalSign);
    }
    
    public SiderEntryList getMeds() {
        return meds;
    }
    public void setMeds(SiderEntryList med) {
        this.meds = med;
    }
    public void addMeds(SiderEntryList med) {
    	if (med != null)
    		this.meds.addAll(med);
    }
    public void addMeds(SiderEntry med) {
        this.meds.add(med);
    }
    
    public int getOrphaNumber() {
        return orphaNumber;
    }
    public void setOrphaNumber(int orphaNumber) {
        this.orphaNumber = orphaNumber;
    }    
    public String getOmimId() {
		return omimId;
	}
	public void setOmimId(String omimId) {
		this.omimId = omimId;
	}

	@Override
    public String toString() {    	
    	return "orpha number: " + orphaNumber + "\tname: " + name + "\tclinical sign: " + clinicalSigns + "\tmeds: " + meds;
    }
    
    public boolean equals(OrphaEntry other) {
    	return (this.orphaNumber == other.getOrphaNumber() &&
    			this.name.equals(other.getName()) &&
    			this.clinicalSigns.equals(other.getClinicalSigns()));
    }
    
}
