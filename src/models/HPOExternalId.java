package models;

public class HPOExternalId {
    String db;
    String id;
    String HPOId;
    String label;
    
    public HPOExternalId(String db, String id, String hPOId, String label) {
        super();
        this.db = db;
        this.id = id;
        HPOId = hPOId;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHPOId() {
        return HPOId;
    }

    public void setHPOId(String hPOId) {
        HPOId = hPOId;
    }
    
    @Override
    public String toString() {
        String string = "";
        
        string += "HPO ID = " + HPOId + "\n";
        string += "DB = " + db + "\n";
        string += "EXT ID = " + id + "\n";
        string += "LABEL = " + label + "\n";
        
        return string;
    }
}
