package models;

import java.util.ArrayList;

public class Drug {

	String cui;
	ArrayList<String> indications, sideEffects, labels; 
	
	public Drug() {
		indications = new ArrayList<String>();
		sideEffects = new ArrayList<String>();
		labels = new ArrayList<String>();
	}

	
	public void addIndication(String ind) {
		if (!indications.contains(ind))
			indications.add(ind);
	}
	public void addIndications(ArrayList<String> inds) {
		if (!indications.containsAll(inds))
			indications.addAll(inds);		
	}
	public void addSideEffect(String se) {
		if (!sideEffects.contains(se))
			sideEffects.add(se);
	}
	public void addSideEffects(ArrayList<String> ses) {
		if (!sideEffects.containsAll(ses))
			sideEffects.addAll(ses);
	}
	public void addLabel(String label) {
		if (!labels.contains(label))
			this.labels.add(label);
	}
	public void addLabels(ArrayList<String> labels) {
		if (!this.labels.containsAll(labels))
			this.labels.addAll(labels);
	}
	
	
	public String getCui() {
		return cui;
	}

	public void setCui(String cui) {
		this.cui = cui;
	}

	public ArrayList<String> getIndications() {
		return indications;
	}

	public void setIndications(ArrayList<String> indications) {
		this.indications = indications;
	}

	public ArrayList<String> getSideEffects() {
		return sideEffects;
	}

	public void setSideEffects(ArrayList<String> sideEffects) {
		this.sideEffects = sideEffects;
	}

	public ArrayList<String> getLabels() {
		return labels;
	}

	public void setLabels(ArrayList<String> labels) {
		this.labels = labels;
	}
	
	@Override
	public String toString() {
		String string = "cui: ";
		
		string += cui + "\nindications: " + indications + "\nside effects: " + sideEffects + "\nlabels: " + labels+"\n";				
		
		return string;
	}
	
}
