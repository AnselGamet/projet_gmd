package models;

import java.util.ArrayList;

public class OmimTxtEntry {

	String id;	
	ArrayList<String> clinicalSymptoms, titles;
	SiderEntryList meds;
	
	public OmimTxtEntry(String id) {
		this.id = id;
		clinicalSymptoms = new ArrayList<String>();
		titles = new ArrayList<String>();
		meds = new SiderEntryList(); 
	}

	public ArrayList<String> getTitles() {
		return titles;
	}
	public void setTitles(ArrayList<String> titles) {
		this.titles = titles;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public ArrayList<String> getClinicalSymptoms() {
		return clinicalSymptoms;
	}
	public void setClinicalSymptoms(ArrayList<String> clinicalSymptoms) {
		this.clinicalSymptoms = clinicalSymptoms;
	}
	
	public SiderEntryList getMeds() {
        return meds;
    }
    public void setMeds(SiderEntryList med) {
        this.meds = med;
    }
    public void addMeds(SiderEntryList med) {
    	if (med != null)
    		this.meds.addAll(med);
    }
    public void addMeds(SiderEntry med) {
        this.meds.add(med);
    }
    

	@Override
	public String toString() {
		String string = "";
		string += "OMIM id : " + id + "\n";
		
		//System.out.println(titles.size());
		for (String s: titles)
			string += "title : " + s + "\n";
		
		for (String s: clinicalSymptoms)
			string += "clinical symptom : " + s + "\n";
		
		System.out.println(meds.size());
		for (SiderEntry s: meds)
			string += "med : " + s + "\n";
		return string;
	}
	
	
}