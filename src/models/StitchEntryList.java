package models;

import java.util.ArrayList;

public class StitchEntryList extends ArrayList<StitchEntry>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//String chemicalId, aliasId, atcId;
	
	public ArrayList<String> getAllChemicalIds() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (StitchEntry se : this) {
			results.add(se.getChemicalId());
		}
		
		return results;
	}
	
	public ArrayList<String> getAllAliasIds() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (StitchEntry se : this) {
			results.add(se.getAliasId());
		}
		
		return results;
	}
	
	public ArrayList<String> getAllAtcIds() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (StitchEntry se : this) {
			if (se != null)
				results.add(se.getAtcId());
		}
		
		return results;
	}
	
	@Override
	public String toString() {
		String string = "";
		for (StitchEntry se:this) {
			string += se + "\n";
		}
		return string;
	}
}
