package models;

import java.util.ArrayList;

public class HPOExternalIdList extends ArrayList<HPOExternalId>{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public HPOExternalIdList getAllOmimIds(){
        HPOExternalIdList omimIds = new HPOExternalIdList();
        
        for (HPOExternalId id:this) {
            if (id.getDb().equals("OMIM"))
                omimIds.add(id);
        }
        
        return omimIds;
    }
    
    public HPOExternalIdList getOrphaIds(){
        HPOExternalIdList orphaIds = new HPOExternalIdList();
        
        for (HPOExternalId id:this) {
            if (id.getDb().equals("ORPHA"))
                orphaIds.add(id);
        }
        
        return orphaIds;
    }
    
    public ArrayList<String> getAllIds(){
        ArrayList<String> ids = new ArrayList<String>();
        
        for (HPOExternalId id:this) {
                ids.add(id.getId());
        }
        
        return ids;
    }
    
    @Override
    public String toString() {
        String string = "";
        
        for (int i = 0; i<this.size()-1; i++)
            string += this.get(i).toString() + "\n";
        
        return string;
    }

}
