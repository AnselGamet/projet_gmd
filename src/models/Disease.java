package models;

import java.util.ArrayList;

public class Disease {

	String OrphaId, OmimId;	
	ArrayList<String> symptoms, names;
	ArrayList<Drug> drugs;
	
	public Disease() {
		OrphaId = "";
		OmimId = "";
		names = new ArrayList<String>();
		symptoms = new ArrayList<String>();
		drugs = new ArrayList<Drug>();
	}

	public void addDrug(Drug d) {
		drugs.add(d);
	}
	public void addDrugs(ArrayList<Drug> ds) {
		if (ds != null)
			drugs.addAll(ds);
	}
	public void addSymptom(String s) {
		symptoms.add(s);
	}
	public void addSymptoms(ArrayList<String> ss) {
		symptoms.addAll(ss);
	}
	public void addName(String n) {
		names.add(n);
	}
	public void addNames(ArrayList<String> ns) {
		names.addAll(ns);
	}
	
	
	public String getOrphaId() {
		return OrphaId;
	}
	public void setOrphaId(String orphaId) {
		OrphaId = orphaId;
	}
	public String getOmimId() {
		return OmimId;
	}
	public void setOmimId(String omimId) {
		OmimId = omimId;
	}
	public ArrayList<String> getSymptoms() {
		return symptoms;
	}
	public void setSymptoms(ArrayList<String> symptoms) {
		this.symptoms = symptoms;
	}
	public ArrayList<String> getNames() {
		return names;
	}
	public void setNames(ArrayList<String> names) {
		this.names = names;
	}
	public ArrayList<Drug> getDrugs() {
		return drugs;
	}
	public void setDrugs(ArrayList<Drug> drugs) {
		this.drugs = drugs;
	}
	
	@Override
	public String toString() {
		String string = "";
		
		string += "Names: " + names + "\n";
		string += "OrphaId: " + OrphaId + "\n";
		string += "OmimId: " + OmimId + "\n";
		string += "Symptoms: " + symptoms + "\n";
		string += "Drugs: " + drugs + "\n";		
		
		return string;
	}
}
