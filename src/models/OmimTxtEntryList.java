package models;

import java.util.ArrayList;

public class OmimTxtEntryList extends ArrayList<OmimTxtEntry>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	public ArrayList<String> getAllTitles() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (OmimTxtEntry ote : this)
			results.addAll(ote.getTitles());
		
		return results;
	}
	
	public ArrayList<String> getAllClinicalSymptoms() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (OmimTxtEntry ote : this)
			results.addAll(ote.getClinicalSymptoms());
		
		return results;
	}
	
	public ArrayList<String> getAllIds() {
		ArrayList<String> results = new ArrayList<String>();
		
		for (OmimTxtEntry ote : this)
			results.add(ote.getId());
		
		return results;
	}
	
	@Override
	public String toString() {
		String string = "";
		for (OmimTxtEntry ote : this) {
			string += ote+"\n";
		}
		return string;
	}
	
}
