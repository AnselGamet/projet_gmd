package models;

public class SiderEntry {

	String sideEffect, cui, stitchId, indication;

	public SiderEntry(String sideEffect, String cui, String stitchId, String indication) {		
		this.sideEffect = sideEffect;
		this.cui = cui;
		this.stitchId = stitchId;
		this.indication = indication;
	}

	public String getSideEffect() {
		return sideEffect;
	}

	public void setSideEffect(String sideEffect) {
		this.sideEffect = sideEffect;
	}

	public String getCui() {
		return cui;
	}

	public void setCui(String cui) {
		this.cui = cui;
	}

	public String getStitchId() {
		return stitchId;
	}

	public void setStitchId(String stitchId) {
		this.stitchId = stitchId;
	}
	
	
	public String getIndication() {
		return indication;
	}

	public void setIndication(String indication) {
		this.indication = indication;
	}

	@Override
	public String toString() {
		return sideEffect + "\t" + cui + "\t" + stitchId;
	}
	
	public boolean equals(SiderEntry other) {
		return (sideEffect.equals(other.getSideEffect()) &&
				cui.equals(other.getCui()) &&
				stitchId.equals(other.getStitchId()));
	}
}
