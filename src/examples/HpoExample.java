package examples;


import interfaces.hpo.IndexHPO;
import interfaces.hpo.SearchHPO;
import interfaces.hpo.SearchHPOAnno;
import models.HPOEntryList;
import models.HPOExternalIdList;

public class HpoExample {

    public static void main(String[] argv) {
        try {
            // indexera le fichier hpo.obo
            IndexHPO.index();
            
            // récupèrera au plus les 5 premieres entrèe similaire à la recherche 
            //HPOEntryList syns = SearchHPO.getNNames("small labia majora",5);
            HPOEntryList syns = SearchHPO.getNNames("Hypoglycemia", 5);
                                  
            System.out.println(syns);    
            
            // initialise la connection à hpo_annotations.sqlite
            SearchHPOAnno HPOanno = new SearchHPOAnno();
            
            // on cherche toute les correspondances à l'id HPO dans ORPHA et OMIM 
            HPOExternalIdList extIds = HPOanno.getAllIds(syns.getAllIds());
            
            System.out.println(extIds);
            
            // on ferme la connection à la db
            HPOanno.close();                              
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
