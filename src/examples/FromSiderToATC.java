package examples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import interfaces.sider.SiderController;
import interfaces.stitch.IndexStitch;
import interfaces.stitch.SearchAtc;
import interfaces.stitch.SearchStitch;
import models.AtcEntry;
import models.StitchEntry;

public class FromSiderToATC {
	
	public static void main(String[] argv) {
		
        String query = "SELECT * FROM meddra_all_indications WHERE cui like 'C3%'";
        List<HashMap<String, Object>> results= SiderController.setQuery(query);
        
        ArrayList<String> compound_id = new ArrayList<String>(); 
        // get id
        if (results.size() > 0) { 
            Set<String> keySet = results.get(0).keySet();
            for (HashMap<String, Object> row : results) {
                for (String key : keySet) {
                	if(key.compareTo("stitch_compound_id1") == 0){
                		//System.out.print(row.get(key));
                    	//System.out.print("\t");
                		compound_id.add((String)row.get(key));
                	}
                }
                //System.out.println();
            }
        }
        
        // REMOVE duplicate

        compound_id=(ArrayList<String>) compound_id.stream().distinct().collect(Collectors.toList());

        
        // Search ds Stich pour recup ref atc
        ArrayList<StitchEntry> inStitch = new ArrayList<StitchEntry>();
        
        for(String i : compound_id){
        	i=SiderController.siderToStichId(i).toLowerCase();
        	try {
				StitchEntry stitchentry = SearchStitch.searchChemId(i);
				if( stitchentry != null)
					inStitch.add(stitchentry);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        

        inStitch=(ArrayList<StitchEntry>) inStitch.stream().distinct().collect(Collectors.toList());

        
        ArrayList<AtcEntry> inAtc = new ArrayList<AtcEntry>();
        
        for(StitchEntry a : inStitch){
        	
        	try {
				AtcEntry atcentry = SearchAtc.searchAtcId(a.getAtcId());
				if(atcentry != null)
					inAtc.add(atcentry);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        }

        inAtc=(ArrayList<AtcEntry>) inAtc.stream().distinct().collect(Collectors.toList());

        
        for(AtcEntry a : inAtc){
        	System.out.println(a.getname());
        }
        
        
         
        
    }

}
