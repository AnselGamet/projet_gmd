package examples;

import java.util.ArrayList;

import org.json.simple.parser.ParseException;

import interfaces.orphadata.SearchOrphadata;
import models.OrphaEntry;

public class OrphaExample {

    public static void main(String[] argv) {    	
        try {
        	ArrayList<OrphaEntry> results = SearchOrphadata.getClinicalSignsById("276556");
        	System.out.println(results.size());
        	for (OrphaEntry od : results) {
				System.out.println(od);
			}        	
        	System.out.println();
        	
        	results = SearchOrphadata.getClinicalSignsByName("Hypoglycemia");
        	System.out.println(results.size());
			for (OrphaEntry od : results) {
				//System.out.println(od);
			}
			System.out.println();
			
        	
        	ArrayList<OrphaEntry> r = SearchOrphadata.getDiseaseById("5");
        	System.out.println(r.size());
			for (OrphaEntry od : r) {
				//System.out.println(od);
			}
			System.out.println();
			
			r = SearchOrphadata.getDiseaseByName("Hypoplastic patellae");	
			System.out.println(r.size());
			for (OrphaEntry od : r) {
				//System.out.println(od);
			}
			System.out.println();
			
			r = SearchOrphadata.getDiseaseByClinicalSign("Hypoglycemia");
			System.out.println(r.size());
			for (OrphaEntry od : r) {
				//System.out.println(od);
			}
			System.out.println();			
		} catch (ParseException e) {
			e.printStackTrace();
		}
        
        
    }
    
}
