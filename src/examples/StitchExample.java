package examples;

import interfaces.stitch.*;
import models.AtcEntry;
import models.StitchEntry;

public class StitchExample {

    public static void main(String[] argv) {
        try {
            
            IndexStitch.index();
            IndexAtc.index();
            
            System.out.println("\n");
            
            StitchEntry resultStitch = SearchStitch.searchChemId("CIDm00000767"); 
            
            System.out.println(resultStitch);
            System.out.println(SearchStitch.searchAliasId("CIDs00028871"));
            System.out.println(SearchStitch.searchAtcId("M01AB10"));
            
            
            AtcEntry resultAtc = SearchAtc.searchAtcId("M01AB10");
            
            System.out.println(resultAtc);
            System.out.println(SearchAtc.searchAtcId("V09IX02"));            
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
}
