package examples;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import interfaces.sider.SiderController;
import models.SiderEntry;

public class SiderExample {

    public static void main(String[] argv) {
    	
    	SiderController.connect();
    	
    	ArrayList<SiderEntry> siderEntries = SiderController.getMedBySideEffect("hypo");
    	
    	for (SiderEntry se : siderEntries) {
    		System.out.println(se);
    	}
    	
    	System.out.println("\nCUI");
    	siderEntries = SiderController.getMedByCui("C1418359");
    	
    	for (SiderEntry se : siderEntries) {
    		System.out.println(se);
    	}
    	

        
        try {
			SiderController.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    
}
