package interfaces.stitch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import utils.Util;

/** Index all text files under a directory.
 * <p>
 * This is a command-line application demonstrating simple Lucene indexing.
 * Run it with no command-line arguments for usage information.
 */
public class IndexAtc {
        
    static String indexPath = "index/atc";
    static String docDir = "./res/atc/atc.keg";
    
    //private IndexStitch() {}

    /** Index all text files under a directory. */
    public static void index() {
        
        boolean create = true;
        Date start = new Date();
        try {
            System.out.println("Indexing to directory '" + indexPath + "'...");

            Directory dir = FSDirectory.open(Paths.get(indexPath));
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

            if (create) {
                iwc.setOpenMode(OpenMode.CREATE);
            } else {
                iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
            }

            IndexWriter writer = new IndexWriter(dir, iwc);
            File file = new File(docDir);
            indexDoc(writer, file);

            writer.close();

            Date end = new Date();
            System.out.println(end.getTime() - start.getTime() + " total milliseconds");

        } catch (IOException e) {
            System.out.println(" caught a " + e.getClass() +
             "\n with message: " + e.getMessage());
        }
    }

    /** Indexes a single document */
    static void indexDoc(IndexWriter writer, File file) throws IOException {
        int eltCount=0;
        if (file.canRead() && !file.isDirectory()) {
            try {
                InputStream ips = new FileInputStream(file);
                InputStreamReader ipsr = new InputStreamReader(ips);
                BufferedReader br = new BufferedReader(ipsr);
                String line;
                boolean finished = false;
                int bracket;
                while((line=br.readLine())!=null && !finished) {
                    
                    if (line.startsWith("E")) {
                        Document doc = new Document();
                        
                        //System.out.print(line.substring(9, 16)+"\t");
                        
                        doc.add(new StringField("atcId", Util.uncapitalize(line.substring(9, 16)), Field.Store.YES));
                        
                        bracket = line.indexOf('[');
                        
                        if (bracket != -1) {
                            //System.out.println(line.substring(17, bracket));
                            doc.add(new TextField("name", line.substring(17, bracket), Field.Store.YES));
                        } else {
                            //System.out.println(line.substring(17));
                            doc.add(new TextField("name", line.substring(17), Field.Store.YES));
                        }
                        
                        //System.out.println(doc.get("atcId") + "\t" + doc.get("name"));
                        
                        eltCount++;
                        writer.addDocument(doc);
                    }
                    
                    
                }
                System.out.println("Number of indexed elements : "+eltCount);
                br.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } 
    }
    
    public static void main(String[] argv) {
        IndexAtc.index();
    }
    
    
}


