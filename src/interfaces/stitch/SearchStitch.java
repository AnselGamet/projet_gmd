package interfaces.stitch;

import java.nio.file.Paths;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import models.StitchEntry;
import models.StitchEntryList;

/** Simple command-line based search demo. */
public class SearchStitch {

  static String index = "index/stitch";
    
  public static StitchEntry search(String field, String text) throws Exception {          
      IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
      IndexSearcher searcher = new IndexSearcher(reader);
      Analyzer analyzer = new StandardAnalyzer();

      QueryParser parser = new QueryParser(field, analyzer);
        
      Query query = parser.parse(text);
      
      //System.out.println("Searching for: " + query.toString(field));
      
      TopDocs results = searcher.search(query, 1);
      ScoreDoc[] hits = results.scoreDocs;      
      
      if (hits.length > 0) {
          Document doc = searcher.doc(hits[0].doc);
          
          String chemicalId = doc.get("chemicalId");
      
          String alias_id = doc.get("aliasId");
      
          String ATC_id = doc.get("atcId");
        
          StitchEntry link = new StitchEntry(chemicalId, alias_id, ATC_id);
          return link;
      }

      reader.close();
      return null;
    }
  
  public static StitchEntry searchChemId(String text) throws Exception {
      return search("chemicalId", text);
  }
  
  public static StitchEntryList searchChemId(ArrayList<String> text) throws Exception {
	  StitchEntryList results = new StitchEntryList();
	  for (String s : text) {
		  results.add(searchChemId(s));
	  }
      return removeDoubles(results);
  }
  
  public static StitchEntry searchAliasId(String text) throws Exception {
      return search("aliasId", text);
  }
  
  public static StitchEntryList searchAliasId(ArrayList<String> text) throws Exception {
	  StitchEntryList results = new StitchEntryList();
	  for (String s : text) {
		  results.add(searchAliasId(s));
	  }
      return removeDoubles(results);
  }
 
  public static StitchEntry searchAtcId(String text) throws Exception {
      return search("atcId", text);
  }
  
  public static StitchEntryList searchAtcId(ArrayList<String> text) throws Exception {
	  StitchEntryList results = new StitchEntryList();
	  for (String s : text) {
		  results.add(searchAtcId(s));
	  }
      return removeDoubles(results);
  }
  
  
  public static StitchEntryList removeDoubles(StitchEntryList sels) {
		//System.out.println("removing doubles");
		
		StitchEntryList results = new StitchEntryList();
		boolean contains;
		
		for (StitchEntry se : sels) {
			if (se != null) {
				contains = false;
				for (StitchEntry se2 : results) {
					if (se.equals(se2)) {
						contains = true;
					}
				}
				if (!contains) {
					
					results.add(se);
				}
			}
		}
		
		return results;
	}
  
}