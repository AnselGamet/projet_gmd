package interfaces.stitch;

import java.nio.file.Paths;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import models.AtcEntry;
import models.AtcEntryList;
import models.AtcEntry;
import models.AtcEntryList;

/** Simple command-line based search demo. */
public class SearchAtc {

  static String index = "index/atc";
    
  public static AtcEntry search(String field, String text) throws Exception {          
      IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
      IndexSearcher searcher = new IndexSearcher(reader);
      Analyzer analyzer = new StandardAnalyzer();

      QueryParser parser = new QueryParser(field, analyzer);
        
      Query query = parser.parse(text);
      
      //System.out.println("Searching for: " + query.toString(field));
      
      TopDocs results = searcher.search(query, 1);
      ScoreDoc[] hits = results.scoreDocs;
      
      if (hits.length > 0) {
          Document doc = searcher.doc(hits[0].doc);
          
          String atcId = doc.get("atcId");
          
          String name = doc.get("name");
              
          AtcEntry link = new AtcEntry(name, atcId);
          return link;
      }

      reader.close();
      return null;
    }
  
  
  public static AtcEntryList searchAtcId(ArrayList<String> text) throws Exception {
	  AtcEntryList results = new AtcEntryList();
	  for (String s : text) {
		  results.add(search("atcId", s));
	  }
      return removeDoubles(results);
  }
  public static AtcEntry searchAtcId(String text) throws Exception {
      return search("atcId", text);
  }
  
  public static AtcEntryList searchName(ArrayList<String> text) throws Exception {
	  AtcEntryList results = new AtcEntryList();
	  for (String s : text) {
		  results.add(search("name", s));
	  }
      return removeDoubles(results);
  }
  public static AtcEntry searchName(String text) throws Exception {
      return search("name", text);
  }  
  
  
  public static AtcEntryList removeDoubles(AtcEntryList sels) {
		//System.out.println("removing doubles");
		
		AtcEntryList results = new AtcEntryList();
		boolean contains;
		
		for (AtcEntry se : sels) {
			if (se != null) {
				contains = false;
				for (AtcEntry se2 : results) {
					if (se.equals(se2)) {
						contains = true;
					}
				}
				if (!contains) {
					
					results.add(se);
				}
			}
		}
		
		return results;
	}
}