package interfaces.sider;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import interfaces.stitch.SearchAtc;
import interfaces.stitch.SearchStitch;
import models.AtcEntry;
import models.SiderEntry;
import models.SiderEntryList;
import models.StitchEntry;

public class SiderController {
	
	private static Connection connection;
	
	public static void connect() {
		if(connection == null) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://neptune.telecomnancy.univ-lorraine.fr/gmd","gmd-read","esial");
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
            }
        }
	}
	
	public static void close() throws SQLException {
		connection.close();
	}
	
	public static List<HashMap<String,Object>> setQuery(String query){					
		PreparedStatement prep;
		//System.out.println(query);
		
		try {
			SiderController.connect();
			prep = connection.prepareStatement(query);
			ResultSet rs = prep.executeQuery();
			
			ResultSetMetaData md = rs.getMetaData();
		    int columns = md.getColumnCount();
		    List<HashMap<String,Object>> list = new ArrayList<HashMap<String,Object>>();

		    while (rs.next()) {
		        HashMap<String,Object> row = new HashMap<String, Object>(columns);
		        for(int i=1; i<=columns; ++i) {
		            row.put(md.getColumnName(i),rs.getObject(i));
		        }
		        list.add(row);
		    }
		    rs.close();
		    return list;
            
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	public static ArrayList<String> getMedocBySideEffect(ArrayList<String> side_effect, int max){
		
		String inquery = "";
		String key1="";
		String key2="";
		side_effect=(ArrayList<String>) side_effect.stream().distinct().collect(Collectors.toList());
		
		int side_effect_size = ( side_effect.size() <= max ) ? side_effect.size() : max;  
		
		for (int i=0;i<side_effect_size;i++){
			if(i != side_effect_size-1)
				inquery=inquery+"'"+side_effect.get(i).toLowerCase()+"',";
			else
				inquery=inquery+"'"+side_effect.get(i).toLowerCase()+"'";
		}
		
		String query = "SELECT * FROM meddra_all_se WHERE side_effect_name in ("+inquery+")";
		//System.out.println("MYSQL QUERY :"+query);
        List<HashMap<String, Object>> results= SiderController.setQuery(query);

        ArrayList<String> compound_id = new ArrayList<String>(); 
        // get id
        if (results.size() > 0) { 
            Set<String> keySet = results.get(0).keySet();
            for (HashMap<String, Object> row : results) {
                for (String key : keySet) {
                	
					if(key.compareTo("stitch_compound_id1") == 0){
                		key1=(String)row.get(key);
                		compound_id.add(key1);
					}
                	if(key.compareTo("stitch_compound_id2") == 0){
                		key2=(String)row.get(key);
                		compound_id.add(key2);
                	}
                	
                	
                }
                

    
                //System.out.println();
            }
        }
        
        // REMOVE duplicate

        compound_id=(ArrayList<String>) compound_id.stream().distinct().collect(Collectors.toList());

        
        // Search ds Stich pour recup ref atc
        ArrayList<StitchEntry> inStitch = new ArrayList<StitchEntry>();
        
        for(String i : compound_id){
        	i=SiderController.siderToStichId(i).toLowerCase();
        	try {
				StitchEntry stitchentry = SearchStitch.searchChemId(i);
				if( stitchentry != null)
					inStitch.add(stitchentry);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        

        inStitch=(ArrayList<StitchEntry>) inStitch.stream().distinct().collect(Collectors.toList());

        
        ArrayList<AtcEntry> inAtc = new ArrayList<AtcEntry>();
        
        for(StitchEntry a : inStitch){
        	
        	try {
				AtcEntry atcentry = SearchAtc.searchAtcId(a.getAtcId());
				if(atcentry != null)
					inAtc.add(atcentry);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        }

        inAtc=(ArrayList<AtcEntry>) inAtc.stream().distinct().collect(Collectors.toList());

        ArrayList<String> meddrasename = new ArrayList<String>();
        
        for(AtcEntry atc  : inAtc){
        	meddrasename.add(atc.getname());
        }
        
        meddrasename.sort(String::compareToIgnoreCase);
        
        return meddrasename;
		
	}
	
	public static ArrayList<String> getMedocCUIByIndication(ArrayList<String> side_effect, int max){
		
		String inquery = "";
		
		side_effect=(ArrayList<String>) side_effect.stream().distinct().collect(Collectors.toList());
		
		int side_effect_size = ( side_effect.size() <= max ) ? side_effect.size() : max;  
		
		for (int i=0;i<side_effect_size;i++){
			if(i != side_effect_size-1)
				inquery=inquery+"'"+side_effect.get(i).toLowerCase().replaceAll("'", "''")+"',";
			else
				inquery=inquery+"'"+side_effect.get(i).toLowerCase().replaceAll("'", "''")+"'";
		}
		
		String query = "SELECT * FROM meddra_all_indications WHERE concept_name in ("+inquery+") OR meddra_concept_name in ("+inquery+")";
		//System.out.println("MYSQL QUERY :"+query);
        List<HashMap<String, Object>> results= SiderController.setQuery(query);
        
        ArrayList<String> compound_id = new ArrayList<String>(); 
        // get id
        if (results.size() > 0) { 
            Set<String> keySet = results.get(0).keySet();
            for (HashMap<String, Object> row : results) {
                for (String key : keySet) {
                	if(key.compareTo("cui") == 0){
                		//System.out.print(row.get(key));
                    	//System.out.print("\t");
                		compound_id.add((String)row.get(key));
                	}
                }
                //System.out.println();
            }
        }
        
        // REMOVE duplicate

        compound_id=(ArrayList<String>) compound_id.stream().distinct().collect(Collectors.toList());

        return compound_id;
	}
	
	public static ArrayList<String> getMedocByCUI(ArrayList<String> side_effect){
		
		String inquery = "";
		String key1="";
		String key2="";
		side_effect=(ArrayList<String>) side_effect.stream().distinct().collect(Collectors.toList());
		
		int side_effect_size =side_effect.size();  
		
		for (int i=0;i<side_effect_size;i++){
			if(i != side_effect_size-1)
				inquery=inquery+"'"+side_effect.get(i).toLowerCase()+"',";
			else
				inquery=inquery+"'"+side_effect.get(i).toLowerCase()+"'";
		}
		
		String query = "SELECT * FROM meddra_all_se WHERE cui in ("+inquery+")";
		//System.out.println("MYSQL QUERY :"+query);
        List<HashMap<String, Object>> results= SiderController.setQuery(query);

        ArrayList<String> compound_id = new ArrayList<String>(); 
        // get id
        if (results.size() > 0) { 
            Set<String> keySet = results.get(0).keySet();
            for (HashMap<String, Object> row : results) {
                for (String key : keySet) {
                	
					if(key.compareTo("stitch_compound_id1") == 0){
                		key1=(String)row.get(key);
                		compound_id.add(key1);
					}
                	if(key.compareTo("stitch_compound_id2") == 0){
                		key2=(String)row.get(key);
                		compound_id.add(key2);
                	}
                	
                	
                }
                

    
                //System.out.println();
            }
        }
        
        // REMOVE duplicate

        compound_id=(ArrayList<String>) compound_id.stream().distinct().collect(Collectors.toList());

        
        // Search ds Stich pour recup ref atc
        ArrayList<StitchEntry> inStitch = new ArrayList<StitchEntry>();
        
        for(String i : compound_id){
        	i=SiderController.siderToStichId(i).toLowerCase();
        	try {
				StitchEntry stitchentry = SearchStitch.searchChemId(i);
				if( stitchentry != null)
					inStitch.add(stitchentry);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        

        inStitch=(ArrayList<StitchEntry>) inStitch.stream().distinct().collect(Collectors.toList());

        
        ArrayList<AtcEntry> inAtc = new ArrayList<AtcEntry>();
        
        for(StitchEntry a : inStitch){
        	
        	try {
				AtcEntry atcentry = SearchAtc.searchAtcId(a.getAtcId());
				if(atcentry != null)
					inAtc.add(atcentry);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        }

        inAtc=(ArrayList<AtcEntry>) inAtc.stream().distinct().collect(Collectors.toList());

        ArrayList<String> meddrasename = new ArrayList<String>();
        
        for(AtcEntry atc  : inAtc){
        	meddrasename.add(atc.getname());
        }
        
        meddrasename.sort(String::compareToIgnoreCase);
        
        return meddrasename;
		
	}
	
	public static SiderEntryList getMedBySideEffect(String name){
		String query = "SELECT * FROM meddra_all_se WHERE side_effect_name LIKE '%"+name+"%' ORDER BY length(side_effect_name) ASC";
		return removeDoubles(queryResultToSiderEntries(SiderController.setQuery(query)));
	}
	public static SiderEntryList getMedBySideEffects(ArrayList<String> names){
		return getMedBySideEffects(names, names.size());
	}
	public static SiderEntryList getMedBySideEffects(ArrayList<String> names, int N){
		SiderEntryList results = new SiderEntryList();
		
		for (int i = 0; i < N && i < names.size(); i++) {			
			results.addAll(getMedBySideEffect(names.get(i)));			      
		}
		return removeDoubles(results);
	}
	
	public static SiderEntryList getMedByIndication(String indic){
		String query = "SELECT * FROM meddra_all_indications WHERE concept_name LIKE '%"+indic+"%' OR meddra_concept_name LIKE '%"+indic+"%' ORDER BY length(concept_name) ASC";
		//System.out.println(query);
		
		SiderEntryList entries = new SiderEntryList();
		List<HashMap<String,Object>> queryResults = SiderController.setQuery(query);
			
		if (queryResults.size() > 0) {
			SiderEntry se;
            for (HashMap<String, Object> row : queryResults) {
            	//System.out.println(row.get("cui"));
            	
            	se = new SiderEntry("",(String) row.get("cui"),siderToStichId((String) row.get("stitch_compound_id")), (String) row.get("concept_name"));
            	//System.out.println(se);
            	entries.add(se);                
            }
        }
		
		return removeDoubles(entries);
	}
	public static SiderEntryList getMedByIndications(ArrayList<String> indics){
		return getMedByIndications(indics, indics.size()-1);
	}
	public static SiderEntryList getMedByIndications(ArrayList<String> indics, int N){
		SiderEntryList results = new SiderEntryList();
		
		for (int i = 0; i < N && i < indics.size(); i++) {
			results.addAll(getMedByIndication(indics.get(i)));			      
		}
		return removeDoubles(results);
	}
	
	
	public static ArrayList<String> getAllId(){
		String query = "SElect * from meddra_all_se";
		List<HashMap<String,Object>> results = setQuery(query);
		
		ArrayList<String> rsl = new ArrayList<String>();
		
        if (results.size() > 0) { 
            Set<String> keySet = results.get(0).keySet();

            for (HashMap<String, Object> row : results) {
                
            	//if(arr[0].compareTo(arr[1]) == 0)
            	
            		rsl.add((String)row.get("stitch_compound_id1"));
            	
            	
            	
            	
                }
                //System.out.println();
            }
        //}
        rsl=(ArrayList<String>) rsl.stream().distinct().collect(Collectors.toList());
		return rsl;
	}
	

	
	public static SiderEntryList getMedByCui(String cui){
		String query = "SELECT * FROM meddra_all_se WHERE side_effect_name LIKE '"+cui+"%' ORDER BY length(side_effect_name) ASC";
		return queryResultToSiderEntries(SiderController.setQuery(query));
	}	
	public static SiderEntryList getMedByCui(ArrayList<String> cuis){
		String query;
		SiderEntryList results = new SiderEntryList();
		
		for (String cui:cuis) {
			query = "SELECT * FROM meddra_all_se WHERE side_effect_name LIKE '"+cui+"%' ORDER BY length(side_effect_name) ASC";
			results.addAll(queryResultToSiderEntries(SiderController.setQuery(query)));
		}
				
		return results;
	}
	
	
	public static SiderEntryList queryResultToSiderEntries(List<HashMap<String,Object>> queryResults) {
		SiderEntryList entries = new SiderEntryList();
		
		if (queryResults.size() > 0) {             
            for (HashMap<String, Object> row : queryResults) {                
            	entries.add(new SiderEntry((String) row.get("side_effect_name"),(String) row.get("cui"),siderToStichId((String) row.get("stitch_compound_id1")), ""));                
            }
        }
		
		return entries;
	}
	
	
	public static SiderEntryList removeDoubles(SiderEntryList sels) {
		//System.out.println("removing doubles");
		
		SiderEntryList results = new SiderEntryList();
		boolean contains;
		
		for (SiderEntry se : sels) {
			if (se != null) {
				contains = false;
				for (SiderEntry se2 : results) {
					if (se.equals(se2)) {
						contains = true;
					}
				}
				if (!contains) {
					
					results.add(se);
				}
			}
		}
		
		return results;
	}
	
	
	public static String siderToStichId(String sider_compound_id){
		String result = null;
		
		if(sider_compound_id.startsWith("CID1")){
			result = "CIDm"+sider_compound_id.substring(4);
		}else{
			result = "CIDs"+sider_compound_id.substring(4);
		}
		return result;
	}
	
	public static String stichToSiderId(String stichid){
		String result = null;
		
		if(stichid.startsWith("CIDm")){
			result = "CID1"+stichid.substring(4);
		}else{
			result = "CID0"+stichid.substring(4);
		}
		
		return result;
	}
	
	

}
