package interfaces;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.    See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/** Index all text files under a directory.
 * <p>
 * This is a command-line application demonstrating simple Lucene indexing.
 * Run it with no command-line arguments for usage information.
 */
public class IndexFiles {
        
    
    private IndexFiles() {}

    /** Index all text files under a directory. */
    public static void main(String[] args) {
        String indexPath = "index/hpo";
        boolean create = true;

        final String docDir = "./res/hpo/hpo.obo";
        
        Date start = new Date();
        try {
            System.out.println("Indexing to directory '" + indexPath + "'...");

            Directory dir = FSDirectory.open(Paths.get(indexPath));
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

            if (create) {
                iwc.setOpenMode(OpenMode.CREATE);
            } else {
                iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
            }

            IndexWriter writer = new IndexWriter(dir, iwc);
            File file = new File(docDir);
            indexDoc(writer, file);

            writer.close();

            Date end = new Date();
            System.out.println(end.getTime() - start.getTime() + " total milliseconds");

        } catch (IOException e) {
            System.out.println(" caught a " + e.getClass() +
             "\n with message: " + e.getMessage());
        }
    }

    /** Indexes a single document */
    static void indexDoc(IndexWriter writer, File file) throws IOException {
        int eltCount=0;
        if (file.canRead() && !file.isDirectory()) {
            try {
                InputStream ips = new FileInputStream(file);
                InputStreamReader ipsr = new InputStreamReader(ips);
                BufferedReader br = new BufferedReader(ipsr);
                String line;
                Boolean termFinished = false;
                int syn_count, alt_id_count;
                while((line=br.readLine())!=null) {
                    if (line.equals("[Term]")) {
                        //System.out.println("New term");
                        Document doc = new Document();
                        termFinished = false;
                        syn_count=0;
                        alt_id_count=0;
                        while(termFinished == false) {
                            line=br.readLine();
                            
                            if (line.equals("")) {
                                System.out.println();
                                termFinished = true;
                                
                            } else if (line.startsWith("id:")) {
                                doc.add(new StringField("id", line.substring(7, line.length()), Field.Store.YES));
                                System.out.println("id: HP:" + doc.get("id"));
                                
                            } else if (line.startsWith("name:")) {
                                doc.add(new TextField("name", line.substring(6, line.length()), Field.Store.YES));
                                System.out.println("name: " + doc.get("name"));
                                
                            } else if (line.startsWith("synonym:")) {
                                doc.add(new TextField("synonym", line.substring(10, line.length()-1).replaceFirst("\"(.)*", ""), Field.Store.YES));
                                System.out.println("synonym: " + doc.getValues("synonym")[syn_count]);
                                syn_count++;
                                
                            } else if (line.startsWith("alt_id:")) {
                                doc.add(new StringField("alt_id", line.substring(11, line.length()), Field.Store.NO));
                                System.out.println("alt_id: HP:" + doc.getValues("alt_id")[alt_id_count]);
                                alt_id_count++;
                                
                            }
                        }
                        writer.addDocument(doc);
                        eltCount++;
                    }
                }
                br.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } 
    }
}

