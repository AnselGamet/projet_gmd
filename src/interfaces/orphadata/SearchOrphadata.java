package interfaces.orphadata;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import models.OmimTxtEntry;
import models.OrphaEntryList;
import models.OrphaEntry;
import models.OrphaEntryList;

public class SearchOrphadata {
	
    static final String DB = "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/diseases/_view/";
	
    public static  ArrayList<String> getRequest(String request){
    	
    	ArrayList<String> rsl= new ArrayList<String>();
    		
    	    try {
    	    	 
    	        String url = request;
    	     
    	        URL obj = new URL(url);
    	        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
    	     
    	        conn.setRequestProperty("Content-Type", "application/json");
    	        conn.setDoOutput(true);
    	        //conn.setDoInput(true);
    	        conn.setRequestMethod("GET");
    	     
    	        String userpass = "martine57u" + ":" + "CouchDB2A";
    	        String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes("UTF-8"));
    	        conn.setRequestProperty ("Authorization", basicAuth);
    	        
    	        BufferedReader reader = new BufferedReader ( new InputStreamReader(conn.getInputStream()));
    	        for (String line; (line = reader.readLine()) != null;) {
    	                //System.out.println(line);
    	                rsl.add(line);
    	        }
    	        reader.close();  
    	     
    	        } catch (Exception e) {
    	        e.printStackTrace();
    	        }
    	    
    	    return rsl;
    	     
    }
    
    
    public static OrphaEntryList getDiseaseById(ArrayList<String> orphaNumbers) throws ParseException {
    	OrphaEntryList results = new OrphaEntryList(); 
    	
    	for (String i:orphaNumbers) {
    		results.addAll(getDiseaseById(i));
    	}
    	
    	return results;
    }
    public static OrphaEntryList getDiseaseById(String orphaNumber) throws ParseException {
        String view = "GetDiseases";
        String parametres = "?key="  + orphaNumber;                 
        ArrayList<String> queryResult = SearchOrphadata.getRequest(DB + view + parametres);
        
        OrphaEntryList results = new OrphaEntryList();
        
        String qr = "";
        for (String s : queryResult) {
        	//System.out.println(s);
        	qr += s;
        }
        
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(qr);			

		JSONArray array = (JSONArray) ((JSONObject)obj).get("rows");		
		int on;
		OrphaEntryList oel;
		OrphaEntry oe;
		String name;
		
		for (int i=0; i<array.size(); i++) {
			JSONObject result = (JSONObject) ((JSONObject) array.get(i)).get("value");
						
			on = Math.toIntExact((long) result.get("OrphaNumber"));			
			oel = getClinicalSignsById(on+"");			
			name = (String) ((JSONObject) result.get("Name")).get("text");
			
			oe = new OrphaEntry(name,  on);
			
			for (OrphaEntry oe_ : oel) {
				oe.addClinicalSigns(oe_.getClinicalSigns());
			}
			
			
			JSONObject extIdss = (JSONObject) result.get("ExternalReferenceList");			
			if (Integer.parseInt((String) extIdss.get("count")) >1) {
				JSONArray extIds = (JSONArray) extIdss.get("ExternalReference");
				for (Object extId : extIds) {
					if ( ((String) ((JSONObject) extId).get("Source")).equals("OMIM") )
						oe.setOmimId(Long.toString((long) ((JSONObject) extId).get("Reference")) );
				}
			} else if (Integer.parseInt((String) extIdss.get("count")) == 1) {
				if ( ((String) ((JSONObject) extIdss.get("ExternalReference")).get("Source")).equals("OMIM") ) {
					oe.setOmimId( Long.toString((long)( (JSONObject) extIdss.get("ExternalReference")).get("Reference")) );
				}
			}
			
			
			results.add(oe);
		}
		
        return results;
    }    
    
    
    public static OrphaEntryList getDiseaseByName(ArrayList<String> names) throws ParseException {
    	OrphaEntryList results = new OrphaEntryList(); 
    	
    	for (String s:names) {
    		results.addAll(getDiseaseByName(s));
    	}
    	
    	return results;
    }    
    public static OrphaEntryList getDiseaseByName(String name) throws ParseException {
        String db = "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/diseases/_view/GetDiseasesByName";
        
        name = name.replaceAll(" ", "%20");        
        String parametres = "?startkey=\""  + name+"\"&endkey=\""+name+"Z\"";         
        ArrayList<String> queryResult = SearchOrphadata.getRequest(db + parametres);
                
        OrphaEntryList results = new OrphaEntryList();
        
        String qr = "";
        for (String s : queryResult) {        	
        	qr += s;
        }
        
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(qr);			

		JSONArray array = (JSONArray) ((JSONObject)obj).get("rows");
		
		String cs;
		int on;
		OrphaEntry oe;
		
		for (int i=0; i<array.size(); i++) {			
			JSONObject result = (JSONObject) ((JSONObject) array.get(i)).get("value");			
			
			//System.out.println(result);
			
			name = (String) ((JSONObject) result.get("Name")).get("text"); 
			cs = "empty";
			
			on = Math.toIntExact((long) result.get("OrphaNumber"));
			
			oe = new OrphaEntry(name, on);
			oe.addClinicalSigns(cs);
			
			results.add(oe);
		}
		
        return results;
    }
    
    
    public static OrphaEntryList getDiseaseByClinicalSign(ArrayList<String> cs) throws ParseException {
    	OrphaEntryList results = new OrphaEntryList(); 
    	
    	for (String s:cs) {
    		results.addAll(getDiseaseByClinicalSign(s));
    	}
    	
    	return results;
    }   
    public static OrphaEntryList getDiseaseByClinicalSign(String cs) throws ParseException {
        String db = "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseByClinicalSign";
        
        cs = cs.replaceAll(" ", "%20");        
        String parametres = "?startkey=\""  + cs+"\"&endkey=\""+cs+"Z\"";
        
        
        ArrayList<String> queryResult = SearchOrphadata.getRequest(db + parametres);               
        
        OrphaEntryList results = new OrphaEntryList();
        
        String qr = "";
        for (String s : queryResult) {   
        	qr += s;
        }
        
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(qr);			

		JSONArray array = (JSONArray) ((JSONObject)obj).get("rows");			
		
		String name;
		int on;
		OrphaEntry oe;
		
		for (int i=0; i<array.size(); i++) {			
			JSONObject result = (JSONObject) ((JSONObject) array.get(i)).get("value");					
			name = (String) ((JSONObject)((JSONObject) result.get("disease")).get("Name")).get("text");
			cs = (String) ((JSONObject) ((JSONObject) result.get("clinicalSign")).get("Name")).get("text");
			on =  Math.toIntExact((long) ((JSONObject) result.get("disease")).get("OrphaNumber")) ;
			
			oe = new OrphaEntry(name, on);
			oe.addClinicalSigns(cs);
			
			results.add(oe);			
		}
		
        return results;
    }
    
    
    public static OrphaEntryList getClinicalSignsById(ArrayList<String> orphaNumbers) throws ParseException {
    	OrphaEntryList results = new OrphaEntryList(); 
    	
    	for (String i:orphaNumbers) {
    		results.addAll(getClinicalSignsById(i));
    	}
    	
    	return results;
    }
    public static OrphaEntryList getClinicalSignsById(String orphaNumber) throws ParseException {
        String db = "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseClinicalSignsNoLang";
        String parametres = "?key="  + orphaNumber; 
        ArrayList<String> queryResult = SearchOrphadata.getRequest(db + parametres);
        
        OrphaEntryList results = new OrphaEntryList();
        
        String qr = "";
        for (String s : queryResult) {
        	qr += s;
        }
        
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(qr);			

		JSONArray array = (JSONArray) ((JSONObject)obj).get("rows");			
		
		String cs, dn;
		int on;
		OrphaEntry oe;
		
		for (int i=0; i<array.size(); i++) {			
			JSONObject result = (JSONObject) ((JSONObject) array.get(i)).get("value");			
			
			cs = (String) ((JSONObject) ((JSONObject) result.get("clinicalSign")).get("Name")).get("text");
			dn = (String) ((JSONObject) ((JSONObject) result.get("disease")).get("Name")).get("text");
			on = Math.toIntExact((long) ((JSONObject) result.get("disease")).get("OrphaNumber"));
			
			oe = new OrphaEntry(dn, on);
			oe.addClinicalSigns(cs);
			
			results.add(oe);
		}
		
        return results;

    }
    
    
    public static OrphaEntryList getClinicalSignsByName(ArrayList<String> cs) throws ParseException {
    	OrphaEntryList results = new OrphaEntryList(); 
    	
    	for (String i:cs) {
    		results.addAll(getClinicalSignsByName(i));
    	}
    	
    	return results;
    }
    public static OrphaEntryList getClinicalSignsByName(String clinicalSign) throws ParseException {
        String db = "http://couchdb.telecomnancy.univ-lorraine.fr/orphadatabase/_design/clinicalsigns/_view/GetDiseaseByClinicalSign";
        String parametres = "?key=\""+ clinicalSign +"\""; 
        ArrayList<String> queryResult = SearchOrphadata.getRequest(db + parametres);
        
        OrphaEntryList results = new OrphaEntryList();
        
        String qr = "";
        for (String s : queryResult) {
        	qr += s;
        }
        
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(qr);			

		JSONArray array = (JSONArray) ((JSONObject)obj).get("rows");			
		
		String cs, dn;
		int on;
		OrphaEntry oe;
		
		for (int i=0; i<array.size(); i++) {			
			JSONObject result = (JSONObject) ((JSONObject) array.get(i)).get("value");			
			
			cs = (String) ((JSONObject) ((JSONObject) result.get("clinicalSign")).get("Name")).get("text");
			dn = (String) ((JSONObject) ((JSONObject) result.get("disease")).get("Name")).get("text");
			on = Math.toIntExact((long) ((JSONObject) result.get("disease")).get("OrphaNumber"));
			
			oe = new OrphaEntry(dn, on);
			oe.addClinicalSigns(cs);
			
			results.add(oe);
		}
		
        return results;

    }
    
    
    public static OrphaEntryList removeDoubles(OrphaEntryList oel) {
		//System.out.println("removing doubles");
		
		OrphaEntryList results = new OrphaEntryList();
		boolean contains;
		
		for (OrphaEntry oe : oel) {
			if (oe != null) {
				contains = false;
				for (OrphaEntry oe2 : results) {										
					if (oe.equals(oe2)) {
						contains = true;
					}
				}
				if (!contains) {
					results.add(oe);
				}
			}
		}
		
		return results;
	}
}
