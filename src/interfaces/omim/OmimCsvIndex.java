package interfaces.omim;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.    See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/** Index all text files under a directory.
 * <p>
 * This is a command-line application demonstrating simple Lucene indexing.
 * Run it with no command-line arguments for usage information.
 */

public class OmimCsvIndex {
        
    static String indexPath = "index/omim_csv";
    static String docDir = "./res/omim/omim_onto.csv";
    
    private OmimCsvIndex() {}

    /** Index all text files under a directory. */
    public static void index() {
        
        boolean create = true;
        Date start = new Date();
        try {
            System.out.println("Indexing to directory '" + indexPath + "'...");

            Directory dir = FSDirectory.open(Paths.get(indexPath));
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

            if (create) {
                iwc.setOpenMode(OpenMode.CREATE);
            } else {
                iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
            }

            IndexWriter writer = new IndexWriter(dir, iwc);
            File file = new File(docDir);                      
            
            indexDoc(writer, file);

            writer.close();

            Date end = new Date();
            System.out.println(end.getTime() - start.getTime() + " total milliseconds");

        } catch (IOException e) {
            System.out.println(" caught a " + e.getClass() +
             "\n with message: " + e.getMessage());
        }
    }

    /** Indexes a single document */
    static void indexDoc(IndexWriter writer, File file) throws IOException {

    	int eltCount=0;
    	String line;

    	if (file.canRead() && !file.isDirectory()) {

    		try {

    			InputStream ips = new FileInputStream(file);
                InputStreamReader ipsr = new InputStreamReader(ips);
                BufferedReader br = new BufferedReader(ipsr);

                //skipping column names line
                line=br.readLine();
                while((line=br.readLine())!=null) {                	
                	String fields[] = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                	
                	int i=0;
                	
                	if (!line.startsWith("http://purl.bioontology.org/ontology/OMIM/MTHU") &&
        					!line.startsWith("http://purl.bioontology.org/ontology/STY/T") &&
        					fields[0].length() < 49){
                		
                		System.out.println(line);
                		Document doc = new Document();
                		
                		doc.add(new StringField("omimId", line.substring(42, 48), Field.Store.YES));
            			
            			doc.add(new TextField("CUI", fields[5], Field.Store.YES));
            			
            			System.out.println("id: OMIM_csv:" + doc.get("omimId") + "\t" +"CUI: OMIM_csv:" + doc.get("CUI"));
                		
            			writer.addDocument(doc);
                    	eltCount++;
                	}                	                	              	
                }
                System.out.println("Number of indexed elements : "+eltCount);
                br.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void main(String[] argv) {
        OmimCsvIndex.index();
    }   
}