package interfaces.omim;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.    See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Paths;
import java.util.Date;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/** Index all text files under a directory.
 * <p>
 * This is a command-line application demonstrating simple Lucene indexing.
 * Run it with no command-line arguments for usage information.
 */

public class OmimTxtIndex {
        
    static String indexPath = "index/omim_txt";
    static String docDir = "./res/omim/omim.txt";
    
    private OmimTxtIndex() {}

    /** Index all text files under a directory. */
    public static void index() {
        
        boolean create = true;
        Date start = new Date();
        try {
            System.out.println("Indexing to directory '" + indexPath + "'...");

            Directory dir = FSDirectory.open(Paths.get(indexPath));
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

            if (create) {
                iwc.setOpenMode(OpenMode.CREATE);
            } else {
                iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
            }

            IndexWriter writer = new IndexWriter(dir, iwc);
            File file = new File(docDir);
            indexDoc(writer, file);

            writer.close();

            Date end = new Date();
            System.out.println(end.getTime() - start.getTime() + " total milliseconds");

        } catch (IOException e) {
            System.out.println(" caught a " + e.getClass() +
             "\n with message: " + e.getMessage());
        }
    }

    /** Indexes a single document */
    static void indexDoc(IndexWriter writer, File file) throws IOException {

    	int eltCount=0;
    	String line;    	

    	if (file.canRead() && !file.isDirectory()) {

    		try {

    			InputStream ips = new FileInputStream(file);
                InputStreamReader ipsr = new InputStreamReader(ips);
                BufferedReader br = new BufferedReader(ipsr);

                while((line=br.readLine())!=null) {
                	//System.out.println(line);
                	
                	// It finishes only with "*RECORD*"
                	Document doc = new Document();
                	do {                		
                		
                        // On récupère l'id OMIM
                		if (line.startsWith("*FIELD* NO")){
                			line=br.readLine();
                			doc.add(new StringField("omimId", line, Field.Store.YES));
                			
                			//System.out.println("omimId : " + doc.get("omimId"));                			
                			
                		// on recupère tout les titres
                		} else if (line.startsWith("*FIELD* TI")){
                			line=br.readLine();
                			String titlesString = line.substring(8);
                			line=br.readLine();
                			while (!line.equals("") && !line.startsWith("*")) {
                				titlesString += line;
                				line=br.readLine();
                			}
                			
                			String[] titles = titlesString.split(";");
                			
                			for (int i = 0; i<titles.length; i++) {
                				if (!titles[i].equals("")) {
                					doc.add(new TextField("omimTitle", titles[i], Field.Store.YES));
                					
                					//System.out.println("omimTitle "+i+" : "+titles[i]);
                				}
                			}                			
                			
            			// on récupère les clinical signs
                		} else if (line.startsWith("*FIELD* CS")){
                			line=br.readLine();
                			while (!line.startsWith("*")){
                				if (line.length() != 0) {                					
	                				if (line.endsWith(":")){
	                					doc.add(new TextField("symptomArea", line.substring(0, line.length()-1), Field.Store.YES));
	                				} else {
	                					if (!line.startsWith("["))
		                					if (line.endsWith(";"))
		                						doc.add(new TextField("clinicalSymptom", line.substring(3, line.length()-1), Field.Store.YES));
		                					else 
		                						doc.add(new TextField("clinicalSymptom", line.substring(3, line.length()), Field.Store.YES));
	                				}
                				}
                				line=br.readLine();
                            }
                        } else if (line.startsWith("*FIELD* TX")) {                        	
                        	String fullDesc = "";
                        	fullDesc += line;
                        	do {
                        		fullDesc += line;
                        		line=br.readLine();                        		                      	
                        	} while (!line.startsWith("*"));
                        	doc.add(new TextField("fullDesc", fullDesc, Field.Store.NO));
                        }
                		
                		
                		
                		line=br.readLine();                		                		
                	} while (line != null && !line.startsWith("*RECORD*"));
                	
                	
                	//System.out.println("OMIM id : "+doc.get("omimId") + "OMIM title : "+doc.get("omimTitle") + "\tClinical Symptom : " + doc.get("clinicalSymptom"));
                	//System.out.println();
                	writer.addDocument(doc);
                    eltCount++;
                    
                }
                System.out.println("Number of indexed elements : "+eltCount);
                br.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void main(String[] argv) {
        OmimTxtIndex.index();
    }   
}