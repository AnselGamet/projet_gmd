package interfaces.omim;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import models.AtcEntry;
import models.AtcEntryList;
import models.OmimTxtEntry;
import models.OmimTxtEntryList;
import models.OmimTxtEntry;
import models.OmimTxtEntryList;

public class SearchOmimTxt {

	static String index = "index/omim_txt";
    
	  public static OmimTxtEntry search(String field, String text) throws Exception {          
	      IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
	      IndexSearcher searcher = new IndexSearcher(reader);
	      Analyzer analyzer = new StandardAnalyzer();

	      QueryParser parser = new QueryParser(field, analyzer);
	        
	      Query query = parser.parse(text);
	      
	      //System.out.println("Searching for: " + query.toString(field));
	      
	      TopDocs results = searcher.search(query, 1);
	      ScoreDoc[] hits = results.scoreDocs;
	      
	      if (hits.length > 0) {
	          Document doc = searcher.doc(hits[0].doc);
	          
	          String OmimId = doc.get("omimId");
	          
	          ArrayList<String> titles = new ArrayList<String>(Arrays.asList(doc.getValues("omimTitle")));
	          ArrayList<String> symptoms = new ArrayList<String>(Arrays.asList(doc.getValues("clinicalSymptom")));
	              
	          OmimTxtEntry link = new OmimTxtEntry(OmimId);
	          link.setTitles(titles);
	          link.setClinicalSymptoms(symptoms);;
	          return link;
	      }

	      reader.close();
	      return null;
	  }
		  
	  
	  public static OmimTxtEntryList searchOmimId(ArrayList<String> text) throws Exception {
		  OmimTxtEntryList results = new OmimTxtEntryList();
		  for (String s : text) {
			  results.add(search("omimId", s));
		  }
	      return results;
	  }
	  public static OmimTxtEntry searchOmimId(String text) throws Exception {
	      return search("omimId", text);
	  }
	  
	  public static OmimTxtEntryList searchTitle(ArrayList<String> text) throws Exception {
		  OmimTxtEntryList results = new OmimTxtEntryList();
		  for (String s : text) {
			  results.add(search("omimTitle", s));
		  }
	      return results;
	  }
	  public static OmimTxtEntry searchTitle(String text) throws Exception {
	      return search("omimTitle", text);
	  }
	  
	  public static OmimTxtEntryList searchClinicalSymptom(ArrayList<String> text) throws Exception {
		  OmimTxtEntryList results = new OmimTxtEntryList();
		  for (String s : text) {
			  results.add(search("clinicalSymptom", s));
		  }
	      return results;
	  }
	  public static OmimTxtEntry searchClinicalSymptom(String text) throws Exception {
	      return search("clinicalSymptom", text);
	  }
	  
	  
	  public static OmimTxtEntryList removeDoubles(OmimTxtEntryList sels) {
			//System.out.println("removing doubles");
			
			OmimTxtEntryList results = new OmimTxtEntryList();
			boolean contains;
			
			for (OmimTxtEntry se : sels) {
				if (se != null) {
					contains = false;
					for (OmimTxtEntry se2 : results) {
						if (se.equals(se2)) {
							contains = true;
						}
					}
					if (!contains) {
						
						results.add(se);
					}
				}
			}
			
			return results;
		}
}
