package interfaces.omim;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import models.OmimCsvEntry;
import models.OmimCsvEntryList;

public class SearchOmimCsv {

	
	static String index = "index/omim_csv";
    
	  public static OmimCsvEntry search(String field, String text) throws Exception {          
	      IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
	      IndexSearcher searcher = new IndexSearcher(reader);
	      Analyzer analyzer = new StandardAnalyzer();

	      QueryParser parser = new QueryParser(field, analyzer);
	        
	      Query query = parser.parse(text);
	      
	      //System.out.println("Searching for: " + query.toString(field));
	      
	      TopDocs results = searcher.search(query, 1);
	      ScoreDoc[] hits = results.scoreDocs;
	      
	      if (hits.length > 0) {
	          Document doc = searcher.doc(hits[0].doc);
	          
	          String omimId = doc.get("omimId");
	          
	          String cui = doc.get("cui");
	          
	              
	          OmimCsvEntry link = new OmimCsvEntry(cui, omimId);
	          
	          return link;
	      }

	      reader.close();
	      return null;
	  }
		  
	  
	  public static OmimCsvEntryList searchOmimId(ArrayList<String> text) throws Exception {
		  OmimCsvEntryList results = new OmimCsvEntryList();
		  for (String s : text) {
			  results.add(search("omimId", s));
		  }
	      return results;
	  }
	  public static OmimCsvEntry searchOmimId(String text) throws Exception {
	      return search("omimId", text);
	  }
	  
	  public static OmimCsvEntryList searchTitle(ArrayList<String> text) throws Exception {
		  OmimCsvEntryList results = new OmimCsvEntryList();
		  for (String s : text) {
			  results.add(search("title", s));
		  }
	      return results;
	  }
	  public static OmimCsvEntry searchTitle(String text) throws Exception {
	      return search("title", text);
	  }
	  
	  public static OmimCsvEntryList searchClinicalSymptom(ArrayList<String> text) throws Exception {
		  OmimCsvEntryList results = new OmimCsvEntryList();
		  for (String s : text) {
			  results.add(search("clinicalSymptom", s));
		  }
	      return results;
	  }
	  public static OmimCsvEntry searchClinicalSymptom(String text) throws Exception {
	      return search("clinicalSymptom", text);
	  }
	
}
