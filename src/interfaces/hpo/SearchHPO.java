/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package interfaces.hpo;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import models.HPOEntry;
import models.HPOEntryList;

/** Simple command-line based search demo. */
public class SearchHPO {

  static String index = "index/hpo";
    
  //private SearchHPO() {}

  public static HPOEntryList search(String field, String text) throws Exception {    
      int numberOfSynonyms = 20;
      
      IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
      IndexSearcher searcher = new IndexSearcher(reader);
      Analyzer analyzer = new StandardAnalyzer();

      BufferedReader in = null;
      in = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
      
      QueryParser parser = new QueryParser(field, analyzer);
        
      Query query = parser.parse(text);
      System.out.println("Searching for: " + query.toString(field));

      HPOEntryList synonyms = getFirstElements(in, searcher, query, numberOfSynonyms);

      reader.close();
      return synonyms;
    }
  
  
  public static HPOEntryList getNNames(String text, int nbrElmts) throws Exception {      
	  
      IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
      IndexSearcher searcher = new IndexSearcher(reader);
      Analyzer analyzer = new StandardAnalyzer();

      BufferedReader in = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
      
      HPOEntryList entries = new HPOEntryList();
      HPOEntry originalEntry = new HPOEntry();
      originalEntry.addName(text);
      originalEntry.setId("-1");      
      //entries.add(originalEntry);      
      
      // We first look for all the entries with the same name 
      String field = "name";
      QueryParser parser = new QueryParser(field, analyzer);
      Query query = parser.parse(text);
      HPOEntryList nameEntries = getFirstElements(in, searcher, query, nbrElmts);

      // We then look for the entries where the name appears in the synonyms
      field = "synonym";
      parser = new QueryParser(field, analyzer);
      query = parser.parse(text);
      HPOEntryList synEntries = getFirstElements(in, searcher, query, nbrElmts);
     
      
      ArrayList<String> names = nameEntries.getAllNames();
      names.addAll(synEntries.getAllNames());
      
      boolean contains = false;
      for (String s : names) {
    	  if (s.equals(text)) {
    		  contains = true;
    		  break;
    	  }
    		  
      }           
    	  
      if (!contains) {
    	 entries.add(originalEntry);
      }
      
      entries.addAll(nameEntries);
      entries.addAll(synEntries);
      
      reader.close();
      
      
      
      
      return entries;
  }
  
  public static HPOEntryList getFirstElements(BufferedReader in, IndexSearcher searcher, Query query, 
          int nbrElmts) throws IOException {
      
      HPOEntryList entries = new HPOEntryList();
      int i = 0;
      Document doc;
      String name;
      String[] intermed;
      
      TopDocs results = searcher.search(query, nbrElmts);
      ScoreDoc[] hits = results.scoreDocs;
      
      //System.out.println(query.toString());
            
      int numTotalHits = results.totalHits;
      //System.out.println(numTotalHits + " total matching documents");
      while (i<numTotalHits && i < hits.length) {
           doc = searcher.doc(hits[i].doc);
           
           name = doc.get("name");
           
           if (name != null) {
               
               entries.add(new HPOEntry());
               
               entries.get(entries.size()-1).setId(doc.get("id"));
               entries.get(entries.size()-1).addName(name);
               
               intermed = doc.getValues("synonym");
               for (String s : intermed) {
                   entries.get(entries.size()-1).addName(s);
               }
               
               intermed = doc.getValues("alt_id");
               for (String s : intermed) {
                   entries.get(entries.size()-1).addAltId(s);
               }
               
           }

           i++;
      }

      return entries;
  }
}