package interfaces.hpo;

import java.sql.*;
import java.util.ArrayList;

import models.HPOExternalId;
import models.HPOExternalIdList; 

public class SearchHPOAnno {
    
    static String dbPath = "./res/hpo/hpo_annotations.sqlite";
    private Connection c;
    private Statement stmt;
    private ResultSet rs;
    
    
    
    public SearchHPOAnno () {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:"+dbPath);
            c.setAutoCommit(false);
            stmt = null;
            rs = null;
            System.out.println("Opened database successfully");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
    }
    
    public void close() {
        try {
        	if (stmt != null)
        		stmt.close();
        	
        	if (rs != null)
        		rs.close();
        	
        	if (c != null)
        		c.close();
            System.out.println("Closed database successfully");
        } catch ( Exception e ) {
            e.printStackTrace();
            //System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
    }
    
    public HPOExternalIdList getOMIMIds(String HPO_id) {
        return getExtIds(HPO_id, "'OMIM'");
    }
    
    public HPOExternalIdList getOMIMIds(ArrayList<String> HPOIds) {
        return getExtIds(HPOIds, "'OMIM'");
    }
    
    public HPOExternalIdList getORPHAIds(String HPO_id) {
        return getExtIds(HPO_id, "'ORPHA'");
    }
    
    public HPOExternalIdList getORPHAIds(ArrayList<String> HPOIds) {
        return getExtIds(HPOIds, "'ORPHA'");
    }
    
    public HPOExternalIdList getAllIds(String HPOId) {
        return getExtIds(HPOId, "'OMIM','ORPHA'");
    }
    
    public HPOExternalIdList getAllIds(ArrayList<String> HPOIds) {    	  
        return getExtIds(HPOIds, "'OMIM','ORPHA'");
    }
    
    public HPOExternalIdList getExtIds(String HPO_id, String db) {
        HPOExternalIdList results = new HPOExternalIdList();
        
        try {
            stmt = c.createStatement();
            String query = "SELECT * FROM phenotype_annotation WHERE sign_id = 'HP:"+HPO_id+"' AND disease_db IN ("+db+");";
            //System.out.println(query+"\n");
            rs = stmt.executeQuery(query);
            
            while ( rs.next() ) {
                String  disease_db = rs.getString("disease_db");
                String  disease_id = rs.getString("disease_id");
                String  disease_label = rs.getString("disease_label");
                String  sign_id = rs.getString("sign_id");
                
                results.add(new HPOExternalId(disease_db, disease_id, sign_id, disease_label));
            }
            
            return results;
        } catch ( Exception e ) {
            //e.printStackTrace();
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        return results;
    }
    
    public HPOExternalIdList getExtIds(ArrayList<String> HPOIds, String dbs) {    	
        HPOExternalIdList results = new HPOExternalIdList();
        
        try {
            stmt = c.createStatement();
            String in = "(";
            
            if (HPOIds.size() == 0) {
            	return null;
            } else if (HPOIds.size() > 1) {
            	for (int i = 0; i<HPOIds.size()-2;i++) {
                    in += "'HP:"+HPOIds.get(i) + "',"; 
                }
                in += "'HP:"+HPOIds.get(HPOIds.size()-1) + "')";
            } else {
            	in += "'HP:"+HPOIds.get(HPOIds.size()-1) + "')";
            }
            
                    
            String query = "SELECT * FROM phenotype_annotation WHERE sign_id IN" +in+ " AND disease_db IN ("+dbs+");";
            System.out.println(query+"\n");
            rs = stmt.executeQuery(query);
            
            while ( rs.next() ) {
                String  disease_db = rs.getString("disease_db");
                String  disease_id = rs.getString("disease_id");
                String  disease_label = rs.getString("disease_label");
                String  sign_id = rs.getString("sign_id");
                
                results.add(new HPOExternalId(disease_db, disease_id, sign_id, disease_label));
            }
            
            return results;
        } catch ( Exception e ) {
            //e.printStackTrace();
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        return results;
    }

}
