package testing;

import java.io.File;
import java.io.FileInputStream;
import java.util.Scanner;

public class Stitch_tests {
    static public void main(String[] args) {
        try {
            File stitch = new File("./res/atc/chemical.sources.v5.0.tsv");
            
            Scanner sc = new Scanner(stitch);
            sc.useDelimiter("\n");
            
            for (int i = 0; i<10000; i++)
                System.out.println(sc.next());
            
            sc.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        

    }
}
