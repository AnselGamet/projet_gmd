package utils;

import java.util.ArrayList;

import models.SiderEntry;
import models.SiderEntryList;

public class Util {
    public static String capitalize(String text) {
        
        String cap = "";
        
        for (char c : text.toCharArray()) {
            cap += Character.toUpperCase(c);
        }
        
        return cap;
    }
    
    public static String capitalize(String text, int start, int length) {
        
        String cap = "";
        
        char[] chars = text.toCharArray();
        
        for (int i = start; i<length; i++) {
            cap += Character.toUpperCase(chars[i]);
        }
        
        return cap + text.substring(length);
    }
    
    public static String uncapitalize(String text) {
        
        String uncap = "";
        
        for (char c : text.toCharArray()) {
            uncap += Character.toLowerCase(c);
        }
        
        return uncap;
    }
    
    public static String uncapitalize(String text, int start, int length) {
        
        String uncap = "";
        
        char[] chars = text.toCharArray();
        
        for (int i = start; i<length; i++) {
            uncap += Character.toLowerCase(chars[i]);
        }
        
        return uncap  + text.substring(length);
    }
}
